
## Running on Docker

```bash
# Build docker image
docker build -t cfdi-be .
# Run image
docker run -p 8080:8080 cfdi-be
```
