# Use official Maven image as base
FROM maven:3.8.7-openjdk-18-slim AS build

# Set the working directory inside the container
WORKDIR /app

# Copy the project's pom.xml to the working directory
COPY pom.xml .

# Download dependencies and cache them in Docker layer
RUN mvn dependency:go-offline -B

# Copy the rest of the project source code to the working directory
COPY src ./src

# Build the application
RUN mvn package -DskipTests

# Use a lightweight base image for the runtime environment
FROM openjdk:17-slim

# Set the working directory inside the container
WORKDIR /app

# Copy the built application JAR file from the build stage
COPY --from=build /app/target/*.jar app.jar

# Expose the port your application listens on
EXPOSE 8080

# Command to run the application
CMD ["java", "-jar", "app.jar"]