package mx.unam.fi.tsic.billapi.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "Customer")
@Data
public class Client {
  @Id
  @OneToOne
  @MapsId("Id")
  @JoinColumn(name = "Id", referencedColumnName = "Id", nullable = false)
  private User user;
}
